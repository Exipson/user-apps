/*
Hey There, da bist du ja wieder. 
Jetzt werden wir deine App ausbauen, und zwar so, dass sie unterscheidet ob ein Junge oder ein Mädchen deinen Channel betreten hat.
Lass und loslegen.
*/

var App = {};

App.onUserJoined = function(user)
{		
	
	// Wir definieren eine neue Variable namens gender, in welcher wir das Geschlecht des neuen Users ablegen.
	// Um zu erfahren welche Informationen du noch aus einem User auslesen kannst, siehst du dir am besten mal die API Dokumentation an. Hier der Link dazu: http://developer.knuddels.de/docs/classes/User.html
	var gender = user.getGender();
	var nick = user.getNick();
	
	// Wir definieren noch eine neue Variable, die je nach Geschlecht des neuen Users eine spezielle Begrüßung enthält.
	var genderGreeting;
	
	// Jetzt wollen wir unterscheiden ob es sich beim neuen User um einen Jungen oder ein Mädchen handelt.
	if (gender == Gender.Male)
	{
		// Falls der User männlich ist, führe alles aus was zwischen diesen geschweiften Klammern steht.
		
		// Der User ist männlich. Lass ihn uns mit dem Wort Prinz begrüßen.
		genderGreeting = 'Prinz ' + nick;	
	}
	else if (gender == Gender.Female)
	{
		// Falls der User weiblich ist, führe alles aus was zwischen diesen geschweiften Klammern steht.
		// Da es im Knuddels System noch User ohne Geschlechtsangabe gibt, können wir nicht einfach if(männlich){tu was} else{tu was} schreiben. 
		
		// Der User ist weiblich. Lass ihn uns mit dem Wort Prinzessin begrüßen.
		genderGreeting = 'Prinzessin ' + nick;		
	}
	else
	{
		// Der User ist weder männlich noch weiblich. Wir können ihn also nicht zugeschnitten auf sein Geschlecht begrüßen.
		genderGreeting = nick;	
	}
	
	var message = 'Hey ' + genderGreeting +  ', willkommen im Channel. Du bist ' + user.getAge() + ' Jahre alt.';
	user.sendPrivateMessage(message);		
};