var App = (new function() {
	
    var knuddelPot;
    var roundTimeout;
    
	this.onAppStart = function()
	{
		refundAllPots('Die App wurde unerwartet beendet und das Spiel konnte nicht korrekt beendet werden.');
        
        startNextRound();
	};
	
	this.onShutdown = function()
	{
		refundAllPots('Die App fährt herunter und das Spiel kann nicht korrekt beendet werden.');
	};
	
	this.onBeforeKnuddelReceived = function(knuddelTransfer)
	{
        if (!knuddelPot)
        {
            knuddelTransfer.reject('Es findet gerade kein Spiel statt.');
        }
        else
        {
            if (knuddelPot.getState() != KnuddelPotState.Open)
            {
                knuddelTransfer.reject('In dieser Runde kann niemand mehr einsteigen.');
            }
            else
            {
                if (knuddelTransfer.canAddToPot(knuddelPot))
                {
                    knuddelTransfer.addToPot(knuddelPot);
                    
                    KnuddelsServer.getDefaultBotUser().sendPublicMessage('Ein neuer Held ist angetreten. Begrüßen Sie mit mir.... °BB°_' + knuddelTransfer.getSender().getProfileLink() + '_°°.');
                }
                else
                {
                    knuddelTransfer.reject('Sorry, das war nicht die richtige Menge Knuddel..');
                }
            }
        }
	};
    
    function startNextRound()
    {

        if (KnuddelsServer.getChannel().getOnlineUsers(UserType.Human).length < 2)
        {
            const millisToNextTry = 60 * 1000;
            setTimeout(startNextRound, millisToNextTry);
        }
        else
        {
            const maxParticipants = 5;
            const knuddelPerRound = 1;
            const maxMillisPerRound = 60 * 1000;

            var bot = KnuddelsServer.getDefaultBotUser();

            knuddelPot = createKnuddelPot(knuddelPerRound, maxParticipants);

            bot.sendPublicMessage('Ab sofort können maximal ' + maxParticipants + ' Helden das Spielfeld betreten. Mit einem _°BB>_hEinsatz von ' + knuddelPerRound + ' Knuddel|/appknuddel ' + bot.getNick().escapeKCode() + ':' + knuddelPerRound + '<°°°_ hast du die Chance deine Fähigkeiten unter Beweis zu stellen.');


            roundTimeout = setTimeout(function () {
                
                if (knuddelPot.getParticipants().length == 0)
                {
                    finishGame();
                }
                else
                {
                    knuddelPot.seal();
                }
            }, maxMillisPerRound);
        }
    }
    
    function finishGame()
    {
        if (roundTimeout)
        {
            clearTimeout(roundTimeout);
            roundTimeout = undefined;
        }

        var participants = knuddelPot.getParticipants();
        var participantCount = participants.length;
        
        if (participantCount == 0)
        {
            knuddelPot.refund();
            knuddelPot = undefined;

            KnuddelsServer.getDefaultBotUser().sendPublicMessage('Niemand hat sich getraut in einen heroischen Kampf zu ziehen. Naja, vielleicht wird\'s ja später nochmal was...');
            
            setTimeout(startNextRound, 15 * 1000);
        }
        else if (participantCount == 1)
        {
            var participant = participants[0];
            
            
            KnuddelsServer.getDefaultBotUser().sendPublicMessage('Keiner hat sich getraut gegen °BB°_' + participant.getProfileLink() + '_°° in einen heroischen Kampf zu ziehen. Das ist traurig.');

            knuddelPot.refund('Da niemand gegen dich in den Kampf ziehen wollte, erhältst du deinen Einsatz zurück.');
            knuddelPot = undefined;
            
            setTimeout(startNextRound, 15 * 1000);
        }
        else
        {
            var winner = RandomOperations.getRandomObject(participants);
            
            knuddelPot.addWinner(winner);
            
            
            // Write a few interesting messages
            KnuddelsServer.getDefaultBotUser().sendPublicMessage('Die Helden haben die Arena betreten und der Kampf beginnt.');
            
            setTimeout(function() {
                KnuddelsServer.getDefaultBotUser().sendPublicMessage('Im Gewirr und Geschwirr hat man kurz Zeit einen Blick auf ' + RandomOperations.getRandomObject(participants) + ' zu ergattern.');

                setTimeout(function() {
                    KnuddelsServer.getDefaultBotUser().sendPublicMessage(RandomOperations.getRandomObject(participants) + ' läuft es kalt den Rücken herunter.');
                    
                    setTimeout(function() {
                        KnuddelsServer.getDefaultBotUser().sendPublicMessage(RandomOperations.getRandomObject(participants) + ' hat das elegante Schwert gezückt.');
                    
                        setTimeout(function() {
                            KnuddelsServer.getDefaultBotUser().sendPublicMessage(winner + ' hat in heroischer Manier den Kampf gewonnen und sammelt den Sold ein.');
                            
                            knuddelPot.payout('Du warst der beste Held und hast den Kampf gewonnen.');
        
                            setTimeout(startNextRound, 15 * 1000);
                        }, 3 * 1000);
                    }, 3 * 1000);
                }, 5 * 1000);
            }, 5 * 1000);
        }
    }
    
    function createKnuddelPot(knuddelPerParticipant, maxParticipants)
    {
        var pot = KnuddelsServer.createKnuddelPot(new KnuddelAmount(knuddelPerParticipant), {
            shouldSealPot: function(pot) {
                return pot.getParticipants().length == maxParticipants;
            },
            onPotSealed: function(pot) {
                finishGame();
            }
        });
        
        pot.setFee(KnuddelsServer.getDefaultBotUser(), 0.05);
        
        return pot;
    }

	function refundAllPots(refundReason)
	{
		var allKnuddelPots = KnuddelsServer.getAllKnuddelPots();
		
		for (var i = 0; i < allKnuddelPots.length; i++)
		{
			var currentKnuddelPot = allKnuddelPots[i];
			
			currentKnuddelPot.refund(refundReason);
		}
	}
    
}());